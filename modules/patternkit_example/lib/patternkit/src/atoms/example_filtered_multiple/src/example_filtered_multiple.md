This is an example Web Component showing content filtered by CKEditor, implemented as a Twig UI Pattern.

It includes the following files:

## example_filtered_multiple.json
The JSON schema file delineates the schema of the pattern. In some cases, libraries may generate these from the Twig Docblock of the pattern Twig template file.

## example_filtered_multiple.md
This file. The text here should appear as pattern documentation in any Style Guide, Pattern Library, or pattern developer tool.

This documentation often will include style rulesets, brand guidelines, designer intention, as well as usage advice.

## example_filtered_multiple.scss
Optional SASS to be compiled to CSS

## example_filtered_multiple.ts
Optional Typescript file to be transpiled to the final JS module. In some libraries, vanilla JS or other variants are used.

## example_filtered_multiple.twig
The HTML template for the pattern. Some libraries may also use Nunjucks, JSX, or Handlebars.

## example_filtered_multiple.yml
Sample data for the pattern. This can be used to generate demo sites, style guide examples, or pre-fill schema editors.

## index.js
The Web Component entry point.
