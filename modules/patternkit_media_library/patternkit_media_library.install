<?php

/**
 * @file
 * Functions run when the module is installed by Drupal.
 */

use Drupal\Core\Link;

/**
 * Implements hook_requirements().
 */
function patternkit_media_library_requirements($phase) {
  $requirements = [];

  $entity_browser_installed = \Drupal::moduleHandler()->moduleExists('entity_browser');
  $media_entity_browser_installed = \Drupal::moduleHandler()->moduleExists('media_entity_browser');
  if (!$entity_browser_installed || !$media_entity_browser_installed) {
    $requirements['patternkit_media_library_recommended_modules'] = [
      'title' => t('Patternkit Media Library Recommended Modules'),
      'description' => t('Patternkit Media Library recommends installing <a href="https://www.drupal.org/project/entity_browser">Entity Browser</a> and <a href="https://www.drupal.org/project/media_entity_browser">Media Entity Browser</a> for the best Media Library Experience.'),
      'severity' => REQUIREMENT_INFO,
    ];
  }

  // This test fails during installation since the route doesn't exist yet to
  // link to.
  if ($phase != 'install') {
    $settings = \Drupal::configFactory()
      ->get('patternkit_media_library.settings');
    $mapping = $settings->get('media_mapping');
    if (empty($mapping) || empty($mapping['image_types']) || empty($mapping['video_types'])) {
      $link = Link::createFromRoute(t('here'), 'patternkit.settings.media_library');

      if (empty($mapping['image_types'])) {
        $requirements['patternkit_media_library_image_media_type_mapping'] = [
          'title' => t('Missing Patternkit Media Library Image Media Type Mapping'),
          'description' => t('Patternkit Media Library settings are missing configuration for selectable image media types. This may be configured @link. Until this is configured, image media will not be selectable in Patternkit blocks.', [
            '@link' => $link->toString(),
          ]),
          'severity' => REQUIREMENT_INFO,
        ];
      }

      if (empty($mapping['video_types'])) {
        $requirements['patternkit_media_library_video_media_type_mapping'] = [
          'title' => t('Missing Patternkit Media Library Video Media Type Mapping'),
          'description' => t('Patternkit Media Library settings are missing configuration for selectable video media types. This may be configured @link. Until this is configured, video media will not be selectable in Patternkit blocks.', [
            '@link' => $link->toString(),
          ]),
          'severity' => REQUIREMENT_INFO,
        ];
      }
    }
  }

  return $requirements;
}

/**
 * Sets default value for new config item 'patternkit_media_library.settings'.
 */
function patternkit_media_library_update_8001(&$sandbox) {
  $config_key = 'patternkit_media_library.settings';

  $config_current = \Drupal::configFactory()->get($config_key);
  // Installs the default config only if this config is not already set.
  if (is_null($config_current->get('use_styles'))) {
    /** @var \Drupal\Core\Config\ConfigInstaller $config_installer_service */
    $config_installer_service = \Drupal::service('config.installer');
    $config_installer_service->installDefaultConfig('module', 'patternkit_media_library');
  }
}

/**
 * Set default values for new media type mapping configuration.
 */
function patternkit_media_library_update_9001() {
  $config_key = 'patternkit_media_library.settings';
  $config_current = \Drupal::configFactory()->getEditable($config_key);

  // Only attempt to set the configuration if there isn't a value yet.
  if (is_null($config_current->get('media_mapping'))) {
    $config = [
      'image_types' => [],
      'video_types' => [],
    ];

    // Guess at configuration based on media types containing either 'image' or
    // 'video' in their id.
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('media');
    foreach (array_keys($bundles) as $type) {
      if (str_contains($type, 'image')) {
        $config['image_types'][] = $type;
      }
      elseif (str_contains($type, 'video')) {
        $config['video_types'][] = $type;
      }
    }

    $config_current->set('media_mapping', $config);
    $config_current->save(TRUE);

    return t('Patternkit Media Library settings have been updated. Please review media type selections on the configuration form.');
  }
}

/**
 * Create new 'patternkit' media view mode for Patternkit media usage.
 */
function patternkit_media_library_update_9002() {
  $repository = \Drupal::service('entity_display.repository');
  $view_modes = $repository->getViewModes('media');

  // Only create the view mode if it doesn't exist already.
  if (empty($view_modes['patternkit'])) {
    $config = [
      "langcode" => "en",
      "status" => TRUE,
      "dependencies" => [
        "module" => [
          "media",
          'patternkit_media_library',
        ],
      ],
      "id" => "media.patternkit",
      "label" => "Patternkit",
      "targetEntityType" => "media",
      "cache" => TRUE,
    ];

    $storage = \Drupal::entityTypeManager()->getStorage('entity_view_mode');
    $view_mode = $storage->create($config);
    $view_mode->save();

    return t('Created new "patternkit" media display mode.');
  }
}

/**
 * Configure the default media embed display mode for Patternkit patterns.
 */
function patternkit_media_library_update_9003() {
  $config_key = 'patternkit_media_library.settings';
  $config_current = \Drupal::configFactory()->getEditable($config_key);

  // Only attempt to set the configuration if there isn't a value yet.
  if (is_null($config_current->get('media_display_mode'))) {
    $config_current->set('media_display_mode', 'patternkit');
    $config_current->save(TRUE);

    return t('Assigned default media display mode for Patternkit to "patternkit".');
  }
}
