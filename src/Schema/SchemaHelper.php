<?php

namespace Drupal\patternkit\Schema;

use Drupal\patternkit\Exception\SchemaValidationException;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\Exception as JsonSchemaException;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\SchemaContract;
use Swaggest\JsonSchema\Structure\ObjectItemContract;

/**
 * A utility class with helper methods for interacting with schemas.
 */
class SchemaHelper {

  /**
   * Identify if a schema uses composition constraints related to traversal.
   *
   * With the assumption that data at this point has already validated, the only
   * composition constraints of concern for traversal where the constraint
   * related to a specific value are:
   * - 'anyOf'
   * - 'oneOf'
   * - 'allOf'
   *
   * @param \Swaggest\JsonSchema\SchemaContract $schema
   *   The schema to be tested.
   *
   * @return bool
   *   TRUE if composition rules are used in the schema. FALSE otherwise.
   */
  public static function isCompositionSchema(SchemaContract $schema): bool {
    foreach (['anyOf', 'oneOf', 'allOf'] as $constraint) {
      // Test like this instead of using isset() or empty() to avoid triggering
      // an exception if we're working with a Wrapper instance.
      // @see \Swaggest\JsonSchema\Wrapper::__isset
      if (property_exists($schema, $constraint) && $schema->$constraint !== NULL) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Identify the schema matching a value in a composed constraint.
   *
   * Composed constraints to be tested include:
   * - 'anyOf'
   * - 'oneOf'
   * - 'allOf'
   *
   * @param \Swaggest\JsonSchema\SchemaContract $schema
   *   The schema to be tested.
   * @param mixed $value
   *   The value being assigned into the specified schema property.
   * @param \Swaggest\JsonSchema\Context|null $context
   *   (Optional) The context to be used during validation. If no context is
   *   provided, the default context will be loaded.
   *
   * @return \Swaggest\JsonSchema\SchemaContract|null
   *   The schema that validates for the given value or NULL if one could not be
   *   identified.
   *
   * @throws \Drupal\patternkit\Exception\SchemaValidationException
   *   Throws an exception if the given value fails to validate against the
   *   provided schema.
   */
  public static function getCompositionSchema(SchemaContract $schema, $value, ?Context $context = NULL): ?SchemaContract {
    try {
      if ($context === NULL) {
        // Get a globally configured execution context for use in schema
        // operations. This will ensure expected options as well as configured
        // remote reference providers and data preprocessors are available.
        $context = \Drupal::service('patternkit.schema.context_builder')
          ->getDefaultContext();
      }

      $validationResult = $schema->in($value, $context);
    }
    catch (InvalidValue $exception) {
      // Set the parent document path for context on the exception.
      $exception->addPath($schema->getDocumentPath());

      // Wrap the underlying exception in our own class to abstract the library
      // in use.
      $wrappedException = new SchemaValidationException($exception->getMessage(), $exception->getCode(), $exception);
      $wrappedException->setSchema($schema);

      throw $wrappedException;
    }

    // Complex values validate and return a schema object, so we can use that to
    // identify which rules were matched.
    if ($validationResult instanceof ObjectItemContract) {
      return static::getConstraintFromObject($validationResult, $schema);
    }
    // Simple values only return the value itself, so we'll need to manually
    // traverse the constraints and identify what applied.
    else {
      return static::manuallyTestConstraints($value, $schema, $context);
    }
  }

  /**
   * Test data against a schema to determine if it validates.
   *
   * This is a helper method to test the validation process while catching
   * thrown exceptions for invalid values.
   *
   * @param \Swaggest\JsonSchema\SchemaContract $schema
   *   The schema to be tested.
   * @param mixed $value
   *   The value to test against the schema.
   * @param \Swaggest\JsonSchema\Context|null $context
   *   (Optional) The context to be used during validation. If no context is
   *   provided, the default context will be loaded.
   *
   * @return bool
   *   TRUE if the value validates for the schema, FALSE otherwise.
   */
  public static function isValid(SchemaContract $schema, $value, ?Context $context = NULL): bool {
    if ($context === NULL) {
      $context = \Drupal::service('patternkit.schema.context_builder')
        ->getDefaultContext();
    }

    $isValid = FALSE;
    try {
      $validationResult = $schema->in($value, $context);
      if ($validationResult) {
        $isValid = TRUE;
      }
    }
    catch (InvalidValue $ignored) {
      // Ignore the exception and leave the validation result as FALSE.
    }
    finally {
      return $isValid;
    }
  }

  /**
   * Parse the applied validation constraint from a validation result object.
   *
   * @param \Swaggest\JsonSchema\Structure\ObjectItemContract $validationResult
   *   The result output from a validation operation.
   * @param \Swaggest\JsonSchema\SchemaContract $schema
   *   The schema used for validation containing the validation constraints.
   *
   * @return \Swaggest\JsonSchema\SchemaContract
   *   The applied validation constraint for the validation result.
   */
  protected static function getConstraintFromObject(ObjectItemContract $validationResult, SchemaContract $schema): SchemaContract {
    // @phpstan-ignore method.notFound
    $documentPath = $validationResult->getDocumentPath();

    // Parse the document path from the object match to identify which schema
    // rule to load as the property schema.
    $parts = explode('->', $documentPath);
    $matches = [];
    preg_match('#^(\w+)\[(\d+)\]$#', $parts[1], $matches);
    [, $rule, $index] = $matches;

    // Load the matching schema rule from the original schema object in order
    // to ensure the full schema is loaded in the case of ref values.
    return ($schema->$rule)[$index];
  }

  /**
   * Manually test constraints to find the applicable rule.
   *
   * @param mixed $data
   *   Test data being applied to the schema.
   * @param \Swaggest\JsonSchema\SchemaContract $schema
   *   The schema being tested against.
   * @param \Swaggest\JsonSchema\Context $context
   *   Schema context for executing validation operations.
   *
   * @return \Swaggest\JsonSchema\SchemaContract
   *   The schema constraint applying to the data for validation.
   *
   * @throws \Drupal\patternkit\Exception\SchemaValidationException
   *   Throws an exception if data doesn't successfully validate for any of the
   *   composition constraints.
   *
   * @todo Evaluate whether allOf needs to be processed separately and aggregated.
   * @todo Remove once upstream issue is resolved.
   * @see https://github.com/swaggest/php-json-schema/issues/143
   */
  protected static function manuallyTestConstraints($data, SchemaContract $schema, Context $context): SchemaContract {
    $constraints = $schema->jsonSerialize();
    $composition = array_merge(
      $constraints->anyOf ?? [],
      $constraints->oneOf ?? [],
      $constraints->allOf ?? [],
    );

    // Return the first composition schema that matches the given value.
    foreach ($composition as $constraint) {
      try {
        if ($constraint->in($data, $context)) {
          return $constraint;
        }
      }
      catch (JsonSchemaException $ignored) {
        // Ignore validation exceptions at this point since we're expecting
        // them until we find one of the constraints that successfully
        // applies.
      }
    }

    // Throw an exception if we didn't find a matching constraint.
    $exception = new SchemaValidationException('Unable to identify valid schema constraint.');
    $exception->setSchema($schema);
    throw $exception;
  }

}
