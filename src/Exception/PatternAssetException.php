<?php

namespace Drupal\patternkit\Exception;

/**
 * Base exception for issues with pattern assets.
 */
class PatternAssetException extends \Exception {}
