<?php

namespace Drupal\patternkit\Exception;

/**
 * An exception to be thrown when an unknown pattern is requested.
 */
class UnknownPatternException extends \LogicException {}
