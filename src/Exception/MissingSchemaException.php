<?php

namespace Drupal\patternkit\Exception;

/**
 * An exception occurring when a pattern schema could not be discovered.
 */
class MissingSchemaException extends PatternAssetException {}
