<?php

namespace Drupal\patternkit\Exception;

/**
 * An exception for when an unknown schema data preprocessor is requested.
 */
class MissingDataPreProcessorException extends \RuntimeException {}
