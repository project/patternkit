<?php

namespace Drupal\patternkit\Exception;

/**
 * An exception occurring when a pattern template could not be discovered.
 */
class MissingTemplateException extends PatternAssetException {}
