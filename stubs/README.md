The files within this directory are used by PHPStan to override or clarify
documented types throughout the codebase. For more information, see the
[related PHPStan documentation](https://phpstan.org/user-guide/stub-files).
